#include "del_space.h"


void del_space(char * buf)
{
							// ������ ����� ���: � buf ����� �������� ������. �������� ���������� first ���� �� buf
	char * first = buf;		// ��������� ������ - �������� ������� second, ���� ������� �� ��������
	char * second = buf;	// ����� ���, ��� �� second �� ����� � ������� first �������� �� ������� ��� \0 � �����.
	int i = 0;


	if( buf[strlen(buf)-1] == '\n') //������ ��� \n � �����
	{
		buf[strlen(buf)-1] = '\0';
	}

	for(i = 0; i < strlen(buf); i++)
	{
		if( (*first == ' ') && ( *(first + 1) == ' ' ))
		{
			second = first;

			while( *second == ' ' )
			{
				second++;
			}

			memmove(first,second - 1, strlen(second-1) + 1); // +1 - ��������  \0. -1 - ��� ����,  ���� ���� ���� ������ �������.
		}

		first++;
	}

	//������ � ��� �� ���� ������ ������ �� ������ �������. ����� ������� ������ � ���������, ���� ��� ����.

	if( buf[0] == ' ') // ������
	{
		memmove( buf, buf+1, strlen( buf + 1) + 1 );
	}

	if( buf[strlen(buf)-1] == ' ') //� ����� ��� � ����� ������?
	{
		buf[strlen(buf)-1] = '\0';
	}

	return;
}